import os
from sklearn.ensemble import GradientBoostingClassifier




os.chdir("/Users/Alexander/desktop/HSE_summer_2016/problems/" + str(12))
test_output = open("test_output.csv", 'r')
train_output = open("train_output.csv", 'r')
test_intput = open("test_input.csv", 'r')
train_intput = open("train_input.csv", 'r')
X = []
Y = []
k = 1
for line in train_intput:
     if line.strip() != '':
        arr = line.strip().split(',')
        for j in range(0, len(arr)):
            try:
                arr[j] = float(arr[j])
            except:
                print(k, arr[j], line)
        X.append(arr)
        k += 1
for line in train_output:
    if line.strip() != '':
        Y.append(int(line.strip()))
testX = []
testY = []
k = 1
for line in test_intput:
    if line.strip() != '':
        arr = line.strip().split(',')
        for j in range(0, len(arr)):
            try:
                arr[j] = float(arr[j])
            except:
                print(k, arr[j], line)
        testX.append(arr)
        k += 1
for line in test_output:
    if line.strip() != '':
        testY.append(int(line.strip()))
results = []
max = 0

for i in range(1, 20):
    for j in range(1, 100):
        clf = GradientBoostingClassifier(max_depth= j, n_estimators= i).fit(X, Y)
        r = clf.score(testX, testY)
        r1 = clf.predict(testX)
        r = round(10000 * r) / 100
        print(r)
        if max < r:
            max = r
            im = i
            jm = j
print(max, im, jm)
