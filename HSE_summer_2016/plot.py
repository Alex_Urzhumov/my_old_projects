from matplotlib.pyplot import mlab
import pylab


def func(x):
    global val
    n = len(val)
    k = 0
    for elem in val:
        if elem <= x:
            k += 1
    return(k / n)

s = (1, 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 24, 27, 28, 29, 31)
best = {1 : 100, 2 : 76.09, 3 : 80.33, 5 : 100, 6 : 100, 7 : 73.33, 8 : 93.33, 9 : 80,
11 : 81.47, 12 : 73.63, 13 : 89.3, 14 : 98.26, 15 : 98, 16 : 85.19, 19 : 57.66, 20 : 54.42,
21 : 90.9, 22 : 100, 24 : 99.1, 27 : 97.6, 28 : 100, 29 : 90.32, 31 : 93.65 }

results = {}
name = input()
for elem in s:
    result = input()
    if len(result) >= 3 and result[2] == ',':
        result = result[:2] + '.' + result[3:]
    result = 100 - float(result)
    if result == (100 - best[elem]) == 0:
        results[elem] = 1
    elif 100 - best[elem] == 0:
        results[elem] = 99
    else:
        results[elem] = round((result / (100 - best[elem]) + 1))
val = sorted(results.values())
print(val)

xmin = 1
xmax = 35
dx = 0.01
xlist = mlab.frange(xmin, xmax, dx)
ylist = [func(arg) for arg in xlist]
pylab.plot(xlist, ylist)
pylab.text(10, 0.5, name)
pylab.savefig('/Users/Alexander/Desktop/HSE_summer_2016/plots/' + name +'.png')
