import os
from sklearn.ensemble import GradientBoostingClassifier

boost_results = open("/Users/Alexander/desktop/HSE_summer_2016/boost_results.txt", 'w')
s = (1, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 24, 27, 28, 29, 31)
for i in s:

    os.chdir("/Users/Alexander/desktop/HSE_summer_2016/problems/" + str(i))
    test_output = open("test_output.csv", 'r')
    train_output = open("train_output.csv", 'r')
    test_intput = open("test_input.csv", 'r')
    train_intput = open("train_input.csv", 'r')

    X = []
    Y = []
    k = 1
    for line in train_intput:
        if line.strip() != '':
            arr = line.strip().split(',')
            for j in range(0, len(arr)):
                try:
                    arr[j] = float(arr[j])
                except:
                    print(k, arr[j], line)
            X.append(arr)
            k += 1
    for line in train_output:
        if line.strip() != '':
            Y.append(int(line.strip()))

    testX = []
    testY = []
    k = 1
    for line in test_intput:
        if line.strip() != '':
            arr = line.strip().split(',')
            for j in range(0, len(arr)):
                try:
                    arr[j] = float(arr[j])
                except:
                    print(k, arr[j], line)
            testX.append(arr)
            k += 1
    for line in test_output:
        if line.strip() != '':
            testY.append(int(line.strip()))

    clf = GradientBoostingClassifier().fit(X, Y)
    r = clf.score(testX, testY)
    r1 = clf.predict(testX)
    r = round(10000 * r) / 100
    print(i, ' ', r)

    boost_results.write(str(i) + '    ' + str(r) + '\n')
boost_results.close()
