import telebot
import config
import requests
import json
import os
import subprocess
from telebot import types

bot = telebot.TeleBot(config.token)
userStep = {}
userCommand = {}
i = 0
size = ''

@bot.message_handler(commands=['start'])
def start_bot(message):
    bot.send_message(message.chat.id, "Hello,\nSend /new to begin\nSend /check to check task\nSend /delete to delete task")
@bot.message_handler(commands=['new'])
def find_file_ids(message):
    keyboard = types.InlineKeyboardMarkup()
    callback_button1 = types.InlineKeyboardButton(text="small", callback_data="small")
    callback_button2 = types.InlineKeyboardButton(text="medium", callback_data="medium")
    callback_button3 = types.InlineKeyboardButton(text="big", callback_data="big")
    keyboard.add(callback_button1, callback_button2, callback_button3)
    bot.send_message(message.chat.id, "Choose your size", reply_markup=keyboard)

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    global size
    if call.message:
        if call.data == "small":
            size = 'small'
        elif call.data == "medium":
            size = 'medium'
        elif call.data == "big":
            size = 'big'
        if size != '':
            userStep[call.message.chat.id] = 1
            bot.send_message(call.message.chat.id, 'You choose ' + size + ' size. Now choose picture and style')
@bot.message_handler(commands=['check'])
def status_check(message):
    userCommand[message.chat.id] = 'check'
    bot.send_message(message.chat.id, 'Please, give me task number')

@bot.message_handler(commands=['delete'])
def status_check(message):
    userCommand[message.chat.id] = 'delete'
    bot.send_message(message.chat.id, 'Please, give me task number')

@bot.message_handler(content_types=["photo"])
def handle_photo(message):
    global size
    global style, picture
    if not (message.chat.id in userStep):
        userStep[message.chat.id] = 0
    if userStep[message.chat.id] == 0:
        bot.send_message(message.chat.id, 'Sorry, but you need to send /new and choose size ')
    if 0 < userStep[message.chat.id] <= 2:
        if len(message.photo) > 1:
            if size == 'small':
                elem = message.photo[1]
            elif size == 'big':
                elem = message.photo[-1]
            else:
                elem = message.photo[(len(message.photo) + 1) / 2]
        else:
            elem = message.photo[0]

        file_info = bot.get_file(elem.file_id)
        url = 'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path)
        name = '/Users/Alexander/desktop/project/images/'
        name += str(elem.file_id) + "." + str(url.split('.')[-1])
        response = requests.get(url, stream=True)
        with open(name, "wb") as handle:
            handle.write(response.content)
        handle.close()

    if userStep[message.chat.id] > 2:
        bot.send_message(message.chat.id, 'I already have picture and style \nIf you want to change, please, write /new')
    elif userStep[message.chat.id] == 1:
        picture = name
        userStep[message.chat.id] += 1
        bot.send_message(message.chat.id, 'Please send me style')
    elif userStep[message.chat.id] ==2 :
        style = name
        userStep[message.chat.id] += 1
        bot.send_message(message.chat.id, 'Now choose quality \n(Number of iteration)')

@bot.message_handler(content_types=["text"])
def text_handler(message):
    if not (message.chat.id in userCommand):
        userCommand[message.chat.id] = '0'
    if userCommand[message.chat.id] == 'check':
        if not(message.text.isdigit()):
            bot.send_message(message.chat.id, "Sorry, this isn't task number, try again")
        else:
            userCommand[message.chat.id] = '0'
            s = get(int(str(message.chat.id) + message.text))
            if s.strip() == 'found':
                bot.send_message(message.chat.id, 'Sorry, this task does not exist')
            elif s.strip() == 'process':
                bot.send_message(message.chat.id, 'I am working with this task now')
            elif s.strip() == 'queue':

                bot.send_message(message.chat.id, 'This task in queue now')
            elif os.path.exists(s.strip()):
                file_photo = open (s.strip(), 'r')
                bot.send_photo(message.chat.id, file_photo)
                file_photo.close()
    elif userCommand[message.chat.id] == 'delete':
        if not(message.text.isdigit()):
            bot.send_message(message.chat.id, "Sorry, this isn't task number, try again")
        else:
            userCommand[message.chat.id] = '0'
            s = delete(int(str(message.chat.id) + message.text))
            bot.send_message(message.chat.id, 'Task ' + message.text + ' is ' + s)

    else:
        if not (message.chat.id in userStep):
            userStep[message.chat.id] = 0
        if userStep[message.chat.id] == 3:
            if (message.text.isdigit()):
                if (1 <= int(message.text) <= 1000):
                    iterations = int(message.text)
                    id = post(message.chat.id, iterations)
                    userStep[message.chat.id] = 0
                    bot.send_message(message.chat.id, 'Task number:  ' + str(id))
                else:
                    bot.send_message(message.chat.id, 'Sorry, but your number must be from 1 to 1000')
            else:
                bot.send_message(message.chat.id, 'Sorry, but you must send a number')
        else:
            bot.send_message(message.chat.id, 'You need to choose picture and style')

def post(id, iterations):
    global style, picture, i
    name = 'curl -i http://localhost:5000/api/tasks -H "Content-Type: application/json" -X POST -d '
    task = json.dumps({'picture' : picture, 'style' : style, 'id' : int(str(id) + str(i)), 'iterations': iterations})
    name += "'" + task + "'"
    posting = subprocess.Popen(name, shell= True, stdout= subprocess.PIPE, stderr= subprocess.PIPE)
    posting.wait()
    os.remove(style)
    os.remove(picture)
    i += 1
    return(str(i - 1))

def get(id):
    name = 'curl -i http://localhost:5000/api/tasks/' + str(id)
    proc = subprocess.Popen(name, shell=True, stdout= subprocess.PIPE, stderr= subprocess.PIPE)
    proc.wait()
    res = proc.communicate()
    line = str(res[0]).split(' ')[-1]
    return line[0:-3]

def delete(id):
    name = 'curl -X DELETE http://localhost:5000/api/tasks/' + str(id)
    proc = subprocess.Popen(name, shell=True, stdout= subprocess.PIPE, stderr= subprocess.PIPE)
    proc.wait()
    return 'deleted'

if __name__ == '__main__':
    bot.polling(none_stop=True)