import os
import time
import subprocess

while True:
    file = open('tasks.txt', 'r')
    lines = file.readlines()
    file.close()
    if len(lines) != 0:
        line = lines[0]
        lines.pop(0)
        with open('tasks.txt', 'w') as file:
            file.writelines(lines)
        file.close()
        if line.strip() != '':
            line = line.strip()
            line = line.split(' ')
            neural = "python neural/neural_style.py --content " + line[1]
            neural += " --styles " + line[3] + " --output " + line[5]
            neural += " --iterations " + str(line[7])
            print(neural)
            file = open('curr_task.txt', 'w')
            file.write(line[9])
            file.close()
            proc = subprocess.Popen([neural], shell=True)
            term = False
            while not (os.path.exists(line[5])):
                if os.path.exists('delete.txt'):
                    os.remove('delete.txt')
                    proc.terminate()
                    term = True
                    break
                else:
                    time.sleep(5)
            if not(term):
                proc.wait()
            file = open('curr_task.txt', 'w')
            file.close()
            if os.path.exists(line[3]):
                os.remove(line[3])
            if os.path.exists(line[1]):
                os.remove(line[1])

    else:
        time.sleep(2)

