#!flask/bin/python

import os
from flask import Flask, request, jsonify, make_response, abort
import shutil

app = Flask(__name__)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.route('/api/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    curtask_file = "curr_task.txt"
    if os.path.exists(curtask_file):
        file = open(curtask_file, 'r')
        line = file.readline()
        line = line.strip()
        file.close()
        if line.isdigit():
            if int(line) == int(task_id):
                file = open(curtask_file, 'w')
                file.close()
                file1 = open('delete.txt', 'w')
                file1.close()
    file = open('tasks.txt', 'r')
    lines = file.readlines()
    file.close()
    for i in range(0, len(lines)):
        line = lines[i].strip()
        line = line.split(' ')
        if int(line[9]) == int(task_id):
             lines.pop(i)
    with open('tasks.txt', 'w') as file:
        file.writelines(lines)
    file.close()

    result_file = os.getcwd() + "/images/result" + str(task_id) + ".jpg"
    if os.path.exists(result_file):
        os.remove(result_file)

    return jsonify({'task': "deleted"})


@app.route('/api/tasks/<int:task_id>', methods=['GET'])
def get_status(task_id):
    result_file = os.getcwd() + "/images/result" + str(task_id) + ".jpg"
    if os.path.exists(result_file):
        return jsonify({'task': "done " + result_file})
    else:
        curtask_file = "curr_task.txt"
        if os.path.exists(curtask_file):
            file = open(curtask_file, 'r')
            line = file.readline()
            file.close()
            line = line.strip()
            if line.isdigit():
                if int(line) == int(task_id):
                    return jsonify({'task': "in process"})
        file = open('tasks.txt', 'r')
        lines = file.readlines()
        file.close()
        if len(lines) != 0:
            for elem in lines:
                task = elem.strip().split(' ')
                if str(task[9]) == str(task_id):
                    return jsonify({'task': "in queue"})
        return jsonify({'task': "not found"})


@app.route('/api/tasks', methods=['POST'])
def create_task():
    if not request.json or not 'picture' in request.json or not 'style' in request.json:
        abort(400)
    task = request.json
    task['output'] = 'images/result' + str(task['id']) + '.jpg'
    if 'iterations' not in task:
        task['iterations'] = 100

    try:
        picture_name = 'picture' + str(task['id']) + '.' + (task['picture'].split('.'))[-1]
        picture_name = (os.path.join(os.getcwd(),'images',picture_name))
        shutil.copy(task['picture'], picture_name)
        task['picture'] = picture_name
        style_name = 'style' + str(task['id']) + '.' + (task['style'].split('.'))[-1]
        style_name = (os.path.join(os.getcwd(),'images',style_name))
        shutil.copy(task['style'], style_name)
        task['style'] = style_name
        if os.path.exists(task['output']):
            os.remove(task['output'])
        line = 'picture ' + task['picture']
        line += ' style ' + task['style']
        line += ' output ' + task['output']
        line += ' iterations ' + str(task['iterations'])
        line += ' id ' + str(task['id'])
        file = open('tasks.txt', 'a')
        file.write(line + '\n')
        file.close()
    except:
        abort(400)
    return jsonify({'task': task}), 201

if __name__ == '__main__':
    app.run(debug=True)
