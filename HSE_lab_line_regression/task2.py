import numpy

train_file = '/Users/macbook/Desktop/line_regression/flats_moscow_mod.txt'
price, totsp, livesp, kitsp, dist, metrdist = numpy.loadtxt(train_file, unpack = True)
t_matrix = numpy.matrix((totsp, livesp, kitsp, dist, metrdist))
matrix = t_matrix.transpose()
n = price.size
linear_func = numpy.linalg.inv(t_matrix * matrix) * t_matrix * (numpy.matrix(price)).transpose()
linear_func.transpose()
for i in range(0, 5):
    linear_func[i, 0] = round(linear_func[i, 0], 2)

f = 'price = '
f += str(linear_func[0, 0]) + ' * totsp '
f += '+ ' + str(linear_func[1, 0]) + ' * livesp '
f += '+ ' + str(linear_func[2, 0]) + ' * kitsp '
f += str(linear_func[3, 0]) + ' * dist '
f += str(linear_func[4, 0]) + ' * metrdist' + '\n'
error = (numpy.linalg.norm(matrix * linear_func - numpy.matrix(price).transpose()) ** 2) / n

file = open('result_2.txt', 'w')
file.write(f)
file.write(str(error))
file.close()