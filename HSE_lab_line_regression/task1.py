import numpy
import pylab
from matplotlib import mlab

def func(x):
    global k, b
    return (k * x + b)

def pol(x):
    global a, k
    res = 0
    for i in range(0,k + 1):
        res += (a[i, 0] * (x ** (k - i)))
    return res
file = open('/Users/macbook/Desktop/line_regression/result.txt', 'w')
train_file = '/Users/macbook/Desktop/line_regression/train.txt'
test_file = '/Users/macbook/Desktop/line_regression/test.txt'
x, y = numpy.loadtxt(train_file, delimiter = ' ', usecols = (0, 1), unpack = True)
test_x, test_y = numpy.loadtxt(test_file, delimiter = ' ', usecols = (0, 1), unpack = True)
n = x.size
test_n = test_x.size
ones = numpy.ones(n)
test_ones = numpy.ones(test_n)

t_matrix = numpy.matrix((x,ones))
matrix = t_matrix.transpose()
linear_func = numpy.linalg.inv(t_matrix * matrix) * t_matrix * (numpy.matrix(y)).transpose()
linear_func.transpose()
k = float(linear_func[0])
b = float(linear_func[1])
error = (numpy.linalg.norm(matrix * linear_func - numpy.matrix(y).transpose()) ** 2) / n
file.write('k = 1  error: ' + str(error) + '\n')
file.write('line function: f(x) = ' + str(round(k,2)) + ' * x + ' + str(round(b,2)) + '\n')
xmin = -20.0
xmax = 20.0
dx = 0.01
xlist = mlab.frange (xmin, xmax, dx)
ylist = [func(arg) for arg in xlist]
pylab.plot (xlist, ylist)
for i in range(0, x.size):
    pylab.plot(x[i],[y[i]], 'ro')
pylab.savefig('/Users/macbook/Desktop/line_regression/line_graph.png')

columns = list()
columns.append(x)
columns.append(ones)
test_columns = list()
test_columns.append(test_x)
test_columns.append(test_ones)
old_test_error = 100000
line = 'k  train error    test error' + '\n'
file.write(line)

for i in range(2, 11):
    new_x = x.copy()
    new_x **= i
    columns.insert(0,new_x)
    t_matrix = numpy.matrix(columns)
    matrix = t_matrix.transpose()

    test_new_x = test_x.copy()
    test_new_x **= i
    test_columns.insert(0,test_new_x)
    test_t_matrix = numpy.matrix(test_columns)
    test_matrix = test_t_matrix.transpose()

    function = numpy.linalg.inv(t_matrix * matrix) * t_matrix * (numpy.matrix(y)).transpose()
    function.transpose()

    sqr_error = (numpy.linalg.norm(matrix * function - numpy.matrix(y).transpose()) ** 2) / n
    test_error = (numpy.linalg.norm(test_matrix * function - numpy.matrix(test_y).transpose()) ** 2) / n

    if old_test_error > test_error:
        k = i
        a = function
        old_test_error = test_error
    file.write(str(i) + ' ' + str(sqr_error) + ' ' + str(test_error) + '\n')

f = 'f(x) = ' + str(round(a[0,0],2)) + ' * x^' + str(k)
for i in range(1,k + 1):
    if a[i] > 0:
        f += ' + ' + str(round(a[i,0],2)) + ' * x^' + str(k - i)
    elif a[i] < 0:
        f += ' - ' + str(abs(round(a[i,0],2))) + ' * x^' + str(k - i)
file.write('best function: ' + f + '\n')

xlist = mlab.frange (-7, 7, dx)
ylist = [pol(arg) for arg in xlist]
pylab.plot (xlist, ylist)
pylab.savefig('/Users/macbook/Desktop/line_regression/graph_of_best_function.png')
file.close()